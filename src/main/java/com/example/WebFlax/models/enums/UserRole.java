package com.example.WebFlax.models.enums;

public enum UserRole {
    USER, ADMIN
}
