package com.example.WebFlax.mapper;

import com.example.WebFlax.dto.UserDto;
import com.example.WebFlax.models.UserEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto map(UserEntity userEntity);
    @InheritInverseConfiguration
    UserEntity map(UserDto userDto);
}
