package com.example.WebFlax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFlaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFlaxApplication.class, args);
	}

}
